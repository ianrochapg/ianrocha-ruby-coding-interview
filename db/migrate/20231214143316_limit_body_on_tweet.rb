class LimitBodyOnTweet < ActiveRecord::Migration[7.0]
  def change
    change_column :tweets, :body, :text, limit: 180
  end
end
