# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text(180)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates_length_of :body, :maximum => 180
end
